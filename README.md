# yet another QR Code generator

This one takes a CSV file as a parameter and creates a PNG image file for each of the entries found in the CSV file.
Optionally a third option can be given in the CSV file, that is a logo to embed in the QR code.

## Running the script

### Requirements

This script requires the `qrcode` module of Python and theirs PIL requirements.
You can install them into a virtual environment from the `requirements.txt` file.

```
python -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

### Command line parameters

```
> python3 qrgenerator.py -h
usage: qrgenerator.py [-h] -i FILENAME [-t]

Analyse gtimelogs

options:
  -h, --help            show this help message and exit
  -i FILENAME, --inputfile FILENAME
                        The filename of the CSV file with URL,imagefilename.png entries.
  -t, --includetext     Shall the URL/text be included in the image?
  -g, --grey            Create QR code in greyscale as well
```

### Example

Assume that we have a file called `fsfe.csv` with a couple of activity URLs of various FSFE activities prepared (see below).
Running the following command will create a QR code for all of these activities.

```
python3 qrgenerator.py -i fsfe.csv
```

## CSV Format

Take for example the following CSV table.

```
http://example.com, qrcode_example, ./fancylogo.png
http://example.com, qrcode
```

Two QR codes will be generated `qrcode_example.png` and `qrcode.png` both with a QR code of `http://example.com`.
One of the two created QR codes will have the `fancylogo.png` file embedded in the center of the QR code.

Note that you don't have to supply the file name ending for the PNG image file, the QR code will be saved in.

## License

The script by itself is published under the MIT license.
But the included *Roboto* file is licensed under the Apache 2.0 license by Google.
However the repository is [REUSE compliant](https://reuse.software) so you can check the licenses of all files accordingly.