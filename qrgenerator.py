# SPDX-FileCopyrightText: 2023 Tobias Diekershoff
#
# SPDX-License-Identifier: MIT

"""
Script to generate many QR code from a CSV file source.
"""

import sys
import argparse
import qrcode
from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.colormasks import SolidFillColorMask
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

def generate_qrcode(url, outfile, logofile=False, printtext=False, greyscale=False):
    """
    Create the QR code

    Parameters
    url (string) ................ url/text that shall be QR encoded
    outfile (string) ............ filename for the generated PNG file
    logofile (boolean|string) ... if not false, the filename for the embedded logo
    printtext (boolean) ......... if true the url will be put as text into the image as well
    greyscale (boolean) ......... if true the QR code will be created in color and in a
                                  greyscale version
    """
    qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H, box_size=10)
    qr.add_data(url)
    qr.make(fit=True)

    if logofile:
        img = qr.make_image(image_factory=StyledPilImage,
                color_mask=SolidFillColorMask(
                    back_color=(255, 255, 255), front_color=(32, 45, 121)),
                embeded_image_path=logofile,
                scale=10)
    else:
        img = qr.make_image(image_factory=StyledPilImage,
                color_mask=SolidFillColorMask(
                    back_color=(255, 255, 255), front_color=(32, 45, 121)),
                scale=10)

    if printtext:
        draw = ImageDraw.Draw(img)
        font = ImageFont.truetype("./Roboto-Regular.ttf", 20)
        draw.text((10, 10), url, (0,0,0), font=font)

    img = img.resize((1200, 1200), Image.Resampling.LANCZOS)
    img.save(outfile+'.png')
    if not greyscale:
        imggrey = img.convert('L')
        imggrey.save(outfile+'_grey.png')

# define the command line arguments
parser = argparse.ArgumentParser(description='Analyse gtimelogs')
parser.add_argument("-i", "--inputfile", dest="filename", required=True,
                    help="The filename of the CSV file with URL,imagefilename.png entries.")
parser.add_argument("-t", "--includetext", dest="printtext", action='store_true',
                    help="Shall the URL/text be included in the image?")
parser.add_argument("-g", "--grey", dest="greyscale", action='store_true',
                    help="Create QR code in greyscale as well")
args = parser.parse_args()

# open the CSV file and loop through all lines to create the QR files

try:
    with open(args.filename, 'r') as CSVfile:
        for line in CSVfile.readlines():
            try:
                url, outfile, logofile = line.split(',')
                logofile = logofile.strip()
            except ValueError:
                url, outfile = line.split(',')
                logofile = False
            generate_qrcode(url.strip(), outfile.strip(), logofile, args.printtext, args.greyscale)
except FileNotFoundError:
    sys.exit(f"File {args.filename} not found.")
